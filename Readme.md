HAMSatTracker (3D)
==================

En este repositorio se encuentran los archivos de diseño en 3D en FreeCAD y archivos para impresión en 3D para la construcción de una antena motorizada.

![Cabeza_motorizada](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgK_n8VM4crlLFQ_T295JpB2tmHwnoBzoqa9S_ZrFeMnOCEDSENHcx1_PmAGvS6aCb7LAnQJAKejka80LPxjl-r6tqx5GqpmKitpCXq0-oIBASNeK04hI2tyfZIkBQ5vr7SiM_eKaxYwMf0lFj-D_GykUxu4ICVz_IECr-UpfXRNbdcSvnu2RYaGlQYTQ/s1918/motores1.png)

![Antena](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEi2PJNqjBsyBuhg4rkSO2s5SVpVOssX1BG44eIiO7114fmyeVr2Q-64tv9cpmgMcy88-xqYbVWxM8S6x8rRw4k9ROL419k0tTyF5WIY4pyu8IdmFSuzZoxGzASCcS_QLjHusPjWj30fNBU912124WxhfvXGfJC06RntwS_Pk3GAmvrO0USP4MUjwFMLzQ/s1918/antena.png)

![Caja_3D](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEjFW2Sb1Mm4PzyIJDBtaKN2C5yPcV2B_2CRUiFr9t6FgRsj_y_6IaebIN-ZwM0S53eUltsT3IZLqp9BLHhq-pgVraxXDc5AxgyCbFDBcrp_SZ5nS7CnjQmZrXgnUsRDHBY6jYPSm-DZO0Ja38-4EkWJ6mubRKTbdKMUIYbXQkPfMhDmtEzntYRyls3dNw/s1918/Caja1.png)

Dentro de cada carpeta se pueden encontrar subcarpetas con diferentes tipos de archivos, su descripción es la siguiente:

- **Carpeta raíz:** suele tener archivos con extensión FCStd, que son los pertenecientes a FreeCAD y contienen las piezas modeladas en 3D.
- **Capeta stl:** contiene los archivos stl con objetos 3D importados de otros, como tornillos, tuercas, etc...
- **Carpeta step:** contiene los archivos step importados de terceros, como rodamientos, motores, etc...
- **Carpeta amf:** contiene los archivos amf, que son cada una de las piezas que se necesitan imprimir para montar el proyecto.
- **Carpeta 3mf:** contiene los archivos 3mf, que incluyen información adicional para su impresión.


Para más información consultar el indice del proyecto en el [blog.](https://www.circuiteando.net/2023/04/hamsattracker-resumen-e-indice.html)
O los artículos relacionados con el [diseño](https://www.circuiteando.net/2023/04/hamsattracker-parte-1-diseno-y-mecanica.html) y la [antena.](https://www.circuiteando.net/2023/04/hamsattracker-parte-3-antena.html)
